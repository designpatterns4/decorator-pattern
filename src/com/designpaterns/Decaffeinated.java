package com.designpaterns;

public class Decaffeinated extends Beverage{
    public Decaffeinated() {
        description= "Decaffeinated Coffee";
    }

    @Override
    public double cost() {
        return 1.05;
    }
}
